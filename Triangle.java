package net.codejava;

public class Triangle extends Figures {
	double height;
	double areaTriangle;

	public Triangle(double h) {
		height = h;
	}

	@Override
	public double empty() {
		areaTriangle = ((storona * height) / 2);
		return areaTriangle;
	}

}